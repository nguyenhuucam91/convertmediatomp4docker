@extends('layout')

@section('title')
    Converter
@endsection

@section('content')
    <h1>Uploaded file</h1>
    <form action="{{ action('ConverterController@store') }}" enctype="multipart/form-data" method="POST">
        @csrf
        <input type="file" name="file"/>
        <button type="submit">Submit</button>
    </form>
@endsection
