@extends('layout')

@section('title')
    Logs
@endsection

@section('content')
  <table class="table">
    <tr>
      <td>File</td>
      <td>Converted file</td>
      <td>Action</td>
    </tr>
  @forelse ($files as $file)
    <tr>
      <td>{{ $file->original_file }}</td>
      <td>{{ $file->converted_file }}</td>
      <td>
          <a href="{{ action('LogController@show', ['id' => $file->id]) }}">View</a>
          <a href="javascript:void(0)" onclick="document.getElementById('log-form-delete-{{ $file->id }}').submit()">Delete</a>
          <form id="log-form-delete-{{ $file->id }}" action="{{ action('LogController@delete', ['id' => $file->id]) }}" method="POST">
              @csrf
              @method('DELETE')
          </form>
      </td>
    </tr>
  @empty
    <tr>
      <td colspan="3">No record found</td>
    </tr>
  @endforelse
  </table>
@endsection
