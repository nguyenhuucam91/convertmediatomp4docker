<?php

namespace App\Http\Controllers;

use App\File;
use Illuminate\Support\Facades\Storage;

class LogController extends Controller
{
    public function index()
    {
        $files = File::all();
        return view('logs.index', compact('files'));
    }

    public function delete($id)
    {
        $file = File::where('id', $id);
        Storage::disk('media')->delete($file->converted_file);
        $file->delete();
        return redirect()->action('LogController@index');
    }

    public function show($id)
    {
        $file = File::where('id', $id)->first();
        return view('logs.show', [
            'fileName' => $file->converted_file
        ]);
    }
}
