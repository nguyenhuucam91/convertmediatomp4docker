<?php

namespace App\Http\Controllers;

use App\Events\MediaSaved;
use App\File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConverterController extends Controller
{
    public function index()
    {
        return view('converter.index');
    }
    /**
     * Store and convert file to mp4
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {
        //receive uploaded file
        $fileUploaded = $request->file('file');
        // store file at server, using media disk filesystem
        $fileUploaded->storeAs(null, $fileUploaded->getClientOriginalName(), 'media');

        $uploadedTime = Carbon::now();

        //call event to begin convert uploaded file
        MediaSaved::dispatch($fileUploaded->getClientOriginalName(), $uploadedTime->timestamp);

        //get file name only, plus adding '.mp4' as extension
        $convertedFileName = pathinfo($fileUploaded->getClientOriginalName(), PATHINFO_FILENAME) . '.mp4';

        File::create([
            'original_file' => $fileUploaded->getClientOriginalName(),
            'converted_file' => $uploadedTime->timestamp . '_' . $convertedFileName,
            'uploaded_at' => $uploadedTime
        ]);

        return 'File is converting';
    }
}
