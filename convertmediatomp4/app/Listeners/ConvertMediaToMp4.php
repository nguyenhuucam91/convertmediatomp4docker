<?php

namespace App\Listeners;

use App\Jobs\ConvertToMp4;

class ConvertMediaToMp4
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        ConvertToMp4::dispatch($event)->onQueue('converter');
    }
}
