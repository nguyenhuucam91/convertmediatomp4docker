<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Pbmedia\LaravelFFMpeg\FFMpegFacade;

class ConvertToMp4 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $event;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($event)
    {
        $this->event = $event;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Rewrite
        $x264 = new \FFMpeg\Format\Video\X264();
        $x264->setAudioCodec('libmp3lame');

        $fileName = pathinfo($this->event->media, PATHINFO_FILENAME);

        FFMpegFacade::fromDisk('media')
            ->open($this->event->media)
            ->export()
            ->toDisk('media')
            ->inFormat($x264)
            ->save($this->event->uploadedAt . "_" . $fileName . ".mp4");

        //delete original file
        Storage::disk('media')->delete($this->event->media);
    }
}
