<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MediaSaved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $media;
    public $uploadedAt;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($media, $uploadedAt)
    {
        $this->media = $media;
        $this->uploadedAt = $uploadedAt;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
