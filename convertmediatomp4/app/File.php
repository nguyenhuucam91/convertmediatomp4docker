<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = ['original_file', 'converted_file', 'uploaded_at'];

    public $timestamps = false;
}
