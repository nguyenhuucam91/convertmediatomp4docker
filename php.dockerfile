FROM devilbox/php-fpm:7.4-prod-0.106

USER root
# Cài đặt composer để cài đặt package
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install ffmpeg và supervisor để quản lí Queue
RUN apt-get update && apt-get install -y ffmpeg && apt-get install -y supervisor

RUN mkdir -p /etc/supervisor/laravel-worker

WORKDIR /etc/supervisor/conf.d

COPY supervisor/laravel-worker.conf .

